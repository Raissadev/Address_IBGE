<!DOCTYPE html>
<html>
<head>
    <title>Address</title>
    <meta charset="utf-8" />
    <link href="<?php echo BASE; ?>css/style.css" rel="stylesheet"/>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,200;0,300;0,400;0,500;0,600;0,700;1,400&display=swap" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="keywords" content="palavras-chave,do,meu,site">
	<meta name="description" content="Descrição do meu website">
    <script src="https://unpkg.com/feather-icons"></script>
</head>
<body>
<header class="marginDownSmall positionRelative">
    <div class="wrap w90 center itemsFlex justSpaceBetween alignCenter">
        <div class="col w30">
            <h1 class="hideDeviceMobile"><a>Raissa<span style="color:#3f4579">Dev</span></a></h1>
            <a class="toggle hideDeviceDesktop"><i data-feather="menu"></i></a>
        </div>
        <div class="col w40 colAsideDeviceSmall hideDeviceMobile">
            <nav class="itemsFlex alignCenter">
                <li><a href="<?php echo BASE; ?>">Home</a></li>
                <li><a href="<?php echo BASE; ?>list-address">List Address</a></li>
                <li><a href="<?php echo BASE; ?>list-address-ibge">List IBGE</a></li>
                <li><a href="<?php echo BASE; ?>register-address-ibge">Add UF</a></li>
                <li><a href="<?php echo BASE; ?>register-address">Add Address</a></li>
            </nav>
        </div>
        <div class="col w30 itemsFlex justEnd alignCenter">
            <?php if(isset($_SESSION['login'])){ ?>
            <h4><?php echo $_SESSION['name'] ?></h4>
            <figure class="imgSmallUser marginLeftSmall">
                <img src="<?php echo BASE; ?>uploads/<?php echo $_SESSION['image']; ?>" />
            </figure>
            <?php } else{ ?>
                <a href="<?php echo BASE; ?>register" class="buttonTwo w25 w100Mobile textCenter">Registrar</a>
                <a href="<?php echo BASE; ?>login" class="button w25 hideDeviceMobile textCenter">Logar</a>
            <?php } ?>
        </div>
    </div>
</header>