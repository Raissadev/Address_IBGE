# Address_IBGE
<h2>Sobre o Projeto</h2>
<p>Sistema de pegar a localidade de cidades baseados no UF, possui sistema de adicionar estado para a database com a API do IBGE, além dos recursos de adicionar, atualizar, deletar e criar um endereço.</p>
<h3>Front-end:</h3>
<ul>
  <li>HTML</li>
  <li>CSS</li>
  <li>JavaScript</li>
</ul>
<h3>Back-end:</h3>
<ul>
  <li>PHP</li>
  <li>Database: MySql</li>
</ul>



![address](https://user-images.githubusercontent.com/82960240/140670432-8e8890b1-ec6b-44cd-8663-0082ba3474b0.gif)


<hr />
<h3>Autor</h3>
<h4>Raissa Arcaro Daros</h4>
<div style="display: inline_block;"><br>
   
[![Blog](https://img.shields.io/badge/Instagram-E4405F?style=for-the-badge&logo=instagram&logoColor=white)](https://www.instagram.com/raissa_dev/)
[![Blog](https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/raissa-dev-69986a214/)
[![Blog](https://img.shields.io/badge/GitHub-100000?style=for-the-badge&logo=github&logoColor=white)](https://github.com/Raissadev/)  
   
</div>
