<section class="welcomeContainer marginDownBigger">
    <div class="wrap w90 center">
        <div class="textContent textCenter">
            <p>All you need soft</p>
            <h2>Explore All Citys</h2>
        </div>
    </div>
    <form method="post" class="row w70 center itemsFlex justSpaceBetween alignCenter boxEffect">
        <div class="col w30 w80Mobile">
            <div class="itemsFlex marginDownSmallIn justSpaceBetween">
                <a class="icon marginRightSmallIn"><i data-feather="map-pin"></i></a>
                <div class="w90 w70Mobile">
                    <h4>Name City</h4>
                    <input type="text" name="name" placeholder="Name City" class="styleNone" autocomplete="off" />
                </div>
            </div>
        </div>
        <div class="col w30 hideDeviceMobile">
            <div class="itemsFlex marginDownSmallIn">
                <a class="icon marginRightSmallIn"><i data-feather="map-pin"></i></a>
                <div class="w90 w70Mobile">
                    <h4>Identifier</h4>
                    <input type="text" name="identifier" placeholder="Id" class="styleNone" autocomplete="off" />
                </div>
            </div>
        </div>
        <div class="col w30 hideDeviceMobile">
            <div class="itemsFlex marginDownSmallIn">
                <a class="icon marginRightSmallIn"><i data-feather="map-pin"></i></a>
                <div class="w90 w70Mobile">
                    <h4>Date</h4>
                    <input type="text" name="date" placeholder="2021-11-07" class="styleNone" autocomplete="off" />
                </div>
            </div>
        </div>
        <div class="colButton w10">
            <button name="search"><i data-feather="search"></i></button>
        </div>
    </form>
</section>

<section class="searchContainer marginDownBigger">
    <div class="wrap w85 center">
        <div class="title marginDownSmall">
            <h2 class="positionRelative w20 center w90Mobile">Últimos Registrados
                <span class="booble"></span>
                <span class="booble"></span>
                <span class="booble"></span>
            </h2>
        </div>
        <ul class="itemsFlex flexWrap">
            <?php 
                $list = \models\ibgeModel::listAddressIBGE("LIMIT 10");        
                foreach($list as $key => $value){
            ?>
            <li><a class="limitLineClampOne"><?php echo $value['name']; ?></a></li>
            <?php } ?>
        </ul>
    </div>
</section>

<section class="containerRefs marginDownBigger">
    <div class="wrap w90 center">
        <div class="textContent itemsFlex alignCenter justSpaceBetween marginDownSmall flexWrapMobile">
            <div class="col w50 w100Mobile marginDownSmallMobile">
                <div class="itemsFlex alignCenter">
                    <span class="line"></span>
                    <h2>Categories</h2>
                </div>
                <p class="paragraphStrong">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris ultrices neque eu feugiat dictum. Maecenas sed viverra est. Vestibulum cursus sapien nec sagittis dapibus.</p>
            </div>
            <div class="col w50 itemsFlex justEnd w100Mobile">
                <a class="btnArrow leftAction marginRightSmallIn"><i data-feather="arrow-left"></i></a>
                <a class="btnArrow rightAction"><i data-feather="arrow-right"></i></a>
            </div>
        </div>
        <div class="slide">
            <ul class="slideUl itemsFlex">
            <?php
                $api = new \models\ibgeModel;
                $uf = isset($_POST['uf']) ? $_POST['uf'] : 43;
                $response = $api->getAddress($uf);

                foreach($response as $key => $value){
            ?>
                <li class="boxItem marginRightSmall textCenter">
                    <figure class="imgItem marginDownSmallIn">
                        <img src="<?php echo BASE; ?>images/banner.jpg" />
                    </figure>
                    <h3><?php echo $value->nome ?></h3>
                </li>
            <?php } ?>
            </ul>
        </div>
    </div>
</section>

<section class="containerApresentation marginDownBigger">
    <div class="wrap w90 center itemsFlex alignCenter flexWrapMobile">
        <div class="col w50 marginRightSmall w100Mobile marginDownSmallMobile">
            <figure>
                <img src="<?php echo BASE; ?>images/bg.png" />
            </figure>
        </div>
        <div class="col w50 w100Mobile">
            <h3 class="title marginDownSmall">RaissaDev lorem i'psum</h3>
            <p class="paragraphStrong">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris ultrices neque eu feugiat dictum. Maecenas sed viverra est. Vestibulum cursus sapien nec sagittis dapibus. Duis cursus velit in malesuada tempus. Proin non lobortis mi. Duis molestie tincidunt sem, sit amet efficitur metus tempus vitae. Donec at gravida nulla, at malesuada quam. In hac habitasse platea dictumst. Donec non lorem non tellus pretium finibus. Duis vitae euismod tortor, ac venenatis enim.</p>
        </div>
    </div>
</section>

<section class="destinations marginTopSmall">
    <div class="wrap w90 center">
        <div class="textContent textCenter marginDownSmall">
            <h2 class="title marginDownSmallIn">Top Destinations</h2>
            <p class="paragraphStrong w40 center w90Mobile">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris ultrices neque eu feugiat dictum.</p>
        </div>
        <div class="items">
            <ul class="itemsFlex flexWrap">
            <?php
                $image = \models\addressModel::imagesRandom();
                $count = count($image); 
                $random = rand(1,$count);
                $list = \models\ibgeModel::listAddressIBGE("LIMIT 10");        
                foreach($list as $key => $value){
            ?>
                <li class="box">
                    <figure>
                        <img class="imgdest" src="<?php echo $image[$random]; ?>" />
                    </figure>
                    <div class="marginDownSmall">
                        <h3 class="marginDownSmallIn"><?php echo $value['name']; ?></h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </div>
                    <div class="itemsFlex alignCenter marginTopSmall">
                        <i data-feather="map-pin" class="marginRightSmallIn"></i>
                        <p class="ref">Brasil, <?php echo $value['id']; ?></p>
                    </div>
                </li>
            <?php } ?>
            </ul>
        </div>
    </div>
</section>


