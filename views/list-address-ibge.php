<section class="destinations marginTopSmall">
    <div class="wrap w90 center">
        <div class="textContent textCenter marginDownSmall">
           <form method="post" class="itemsFlex">
               <input type="text" placeholder="Pesquisar" name="uf" class="w90 marginRightSmall" autocomplete="off" />
               <button type="submit" class="w10"><i data-feather="search"></i></button>
           </form>
        </div>
        <div class="items">
            <ul class="itemsFlex flexWrap">
            <?php 
                $uf = isset($_POST['uf']) ? $_POST['uf'] : 43;
                $api = new \models\ibgeModel;
                $response = $api->getAddress($uf);
        
                foreach($response as $key => $value){  
            ?>
                <li class="box">
                    <figure>
                        <img src="<?php echo BASE; ?>images/banner.jpg" />
                    </figure>
                    <div class="marginDownSmall">
                        <h3 class="marginDownSmallIn"><?php echo $value->nome; ?></h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </div>
                    <div class="itemsFlex alignCenter marginTopSmall">
                        <i data-feather="map-pin" class="marginRightSmallIn"></i>
                        <p class="ref">Brasil, <?php echo $value->id; ?></p>
                    </div>
                </li>
            <?php } ?>
            </ul>
        </div>
    </div>
</section>