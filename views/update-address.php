<?php
        $api = new \models\addressModel;
        $response = $api->updateAddressUser();
        \models\userModel::getInfosUser();
?>

<section class="containerInfo itemsFlex alignCenter justCenter">
    <div class="wrap w90 boxEffect">
        <div class="text w50 marginDownSmall w100Mobile">
            <h2 class="title marginDownSmallIn">Update State In The Database</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam quis dui felis. Integer pulvinar auctor quam. In hac habitasse platea dictumst. Ut id bibendum mauris.</p>
        </div>
        <form method="get">
            <input type="text" name="public_place" value="<?php echo $_SESSION['public_place']; ?>" class="w100 marginDownSmallIn" autocomplete="off"  />
            <input type="text" name="number" value="<?php echo $_SESSION['number']; ?>" class="w100 marginDownSmallIn" autocomplete="off"  />
            <input type="text" name="district" value="<?php echo $_SESSION['district']; ?>" class="w100 marginDownSmallIn" autocomplete="off"  />
            <input type="text" name="id_city" value="<?php echo $_SESSION['id_city']; ?>" class="w100 marginDownSmallIn" autocomplete="off"  />
            <button name="update" class="w20 w50Mobile">Update</button>
        </form>
        <div class="marginTopSmall textRight">
            <p>Don't have a state? <a href="<?php echo BASE; ?>register-address">Create!</a></p>
        </div>
    </div>
</section>