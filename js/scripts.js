//ICON ACTIVE
feather.replace();

//Menu
var toggleMenu = document.querySelectorAll('.toggle');
var aside = document.querySelector('.colAsideDeviceSmall');

for(i = 0; i < toggleMenu.length; i++){
    toggleMenu[i].addEventListener('click', menuAction);
}

function menuAction(){
    if(aside.classList.contains('hideDeviceMobile')){
        aside.classList.remove('hideDeviceMobile');
        aside.animate([
            { transform: 'translateX(0)' },
            { transform: 'translateY(10%)' }
          ], {
            duration: 300,
          });
    }else{
        aside.classList.add('hideDeviceMobile');
    }
}

//Slider

const sliderElm = document.querySelector(".slideUl");
const btnLeft = document.querySelector(".leftAction");
const btnRight = document.querySelector(".rightAction");

const numberSliderBoxs = sliderElm.children.length;
let idxCurrentSlide = 0;

// Functions:
function moveSlider() {
  let leftMargin = (sliderElm.clientWidth / 15) * idxCurrentSlide;
  sliderElm.style.marginLeft = -leftMargin + "px";
  console.log(sliderElm.clientWidth, leftMargin);
}
function moveLeft() {
  if (idxCurrentSlide === 0) idxCurrentSlide = numberSliderBoxs - 1;
  else idxCurrentSlide--;

  moveSlider();
}
function moveRight() {
  if (idxCurrentSlide === numberSliderBoxs - 1) idxCurrentSlide = 0;
  else idxCurrentSlide++;

  moveSlider();
}

// Event Listeners:
btnLeft.addEventListener("click", moveLeft);
btnRight.addEventListener("click", moveRight);
window.addEventListener("resize", moveSlider);


