<?php
    \models\addressModel::deleteAddressUser();
?>
<section class="destinations marginTopSmall">
    <div class="wrap w90 center">
        <div class="textContent textCenter marginDownSmall">
           <form method="post" class="itemsFlex">
               <input type="text" placeholder="Pesquisar" name="name" class="w90 marginRightSmall" autocomplete="off" />
               <button type="submit" name="search" class="w10"><i data-feather="search"></i></button>
           </form>
        </div>
        <div class="items">
            <ul class="itemsFlex flexWrap">
            <?php 
                $address = \models\addressModel::getAddressUsers();
                foreach($address as $key => $value){  
            ?>
                <li class="box">
                    <?php
                        if($value['id_user'] == $_SESSION['id']){
                    ?>
                        <a href="?delete" class="iconDelete"><i data-feather="x"></i></a>
                    <?php } ?>
                    <figure>
                        <img src="<?php echo BASE; ?>images/banner.jpg" />
                    </figure>
                    <div class="marginDownSmall">
                        <h3 class="marginDownSmallIn"><?php echo $value['public_place']; ?></h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                    </div>
                    <div class="itemsFlex alignCenter marginTopSmall">
                        <i data-feather="map-pin" class="marginRightSmallIn"></i>
                        <p class="ref">Brasil, <?php echo $value['district'] ?></p>
                    </div>
                </li>
            <?php } ?>
            </ul>
        </div>
    </div>
</section>