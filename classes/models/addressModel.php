<?php

    namespace models;

    class addressModel{

        private $endpoint;
        private $url;

        public function postAddressUser(){
            $db = \MySql::connect()->prepare("SELECT * FROM `address`");
            $db->execute();
            
            if(isset($_GET['register'])){
                $public_place = $_GET['public_place'];
                $number = $_GET['number'];
                $district = $_GET['district'];
                $id_city = $_GET['id_city'];
                $id_user = $_SESSION['id'];
                if($db->rowCount() == 1){
                    echo "<script> alert('Esse endereço já existe!'); </script>";
                    return;
                }
                $insertAddress = \MySql::connect()->prepare("INSERT INTO `address` VALUES (null,?,?,?,?,?)");
                $insertAddress->execute(array($public_place,$number,$district,$id_city,$id_user));
                echo "<script> alert('Endereço registrado com sucesso!'); </script>";
                header('Location: '.BASE.'register-address');
            }

        }

        public function updateAddressUser(){
            if(isset($_GET['update'])){
                $public_place = $_GET['public_place'];
                $number = $_GET['number'];
                $district = $_GET['district'];
                $id_city = $_GET['id_city'];
                $id_user = $_SESSION['id'];
                $updateAddress = \MySql::connect()->prepare("UPDATE `address` SET public_place = ? , number = ? , district = ? , id_city = ?, date = ? WHERE id_user = $id_user");
                $updateAddress->execute(array($public_place,$number,$district,$id_city,date('Y-m-d')));
                echo "<script> alert('Endereço atualizado com sucesso!'); </script>";
                header('Location: '.BASE.'update-address');
            }

        }

        public static function deleteAddressUser(){
            if(isset($_GET['delete'])){
                $idUser = $_SESSION['id'];
                $updateAddress = \MySql::connect()->prepare("DELETE FROM `address` WHERE id_user = $idUser");
                $updateAddress->execute();
                echo "<script> alert('Endereço excluido com sucesso!'); </script>";
                header('Location: '.BASE.'');
            }

        }

        public static function getAddressUsers(){
            $query = '';
            if(isset($_POST['search'])){
                $name = $_POST['name'];
                $query = "WHERE public_place LIKE '$name%' OR district LIKE '$name%'";
            }
            $address = \MySql::connect()->prepare("SELECT * FROM `address` $query");
            $address->execute();
            $address = $address->fetchAll();
            return $address;
        }

        public static function imagesRandom(){
            $image = array(); 
            $image[1] = BASE."images/banner.jpg"; 
            $image[2] = BASE."images/city.png"; 
            $image[3] = BASE."images/newbg.jpg"; 
            return $image;
        }

    }

?>