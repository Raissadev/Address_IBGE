<?php
        $api = new \models\addressModel;
        $response = $api->postAddressUser();
?>

<section class="containerInfo itemsFlex alignCenter justCenter">
    <div class="wrap w90 boxEffect">
        <div class="text w50 marginDownSmall w100Mobile">
            <h2 class="title marginDownSmallIn">Register Address In The Database</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam quis dui felis. Integer pulvinar auctor quam. In hac habitasse platea dictumst. Ut id bibendum mauris.</p>
        </div>
        <form method="get">
            <input type="text" name="public_place" class="w100 marginDownSmallIn" autocomplete="off" />
            <input type="text" name="number" class="w100 marginDownSmallIn" autocomplete="off" />
            <input type="text" name="district" class="w100 marginDownSmallIn" autocomplete="off" />
            <input type="text" name="id_city" class="w100 marginDownSmallIn" autocomplete="off" />
            <button name="register" class="w20 w50Mobile">Create</button>
        </form>
        <div class="marginTopSmall textRight">
            <p>Already own a state? <a href="<?php echo BASE; ?>update-address">Update!</a></p>
        </div>
    </div>
</section>