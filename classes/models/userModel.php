<?php

    namespace models;

    class userModel{

        public static function register($name,$password,$image){
            $register = \MySql::connect()->prepare("INSERT INTO `users` VALUES (null,?,?,?)");
            $register->execute(array($name,$password,$image['name']));
            move_uploaded_file($image['tmp_image'], BASE_UPLOADS.$image['name']);
            header('Location: '.BASE.'');
        }

        public static function login($name,$password){
            $login = \MySql::connect()->prepare("SELECT * FROM `users` WHERE name = ? AND password = ?");
            $login->execute(array($name,$password));
            if($login->rowCount() == 1){
                $info = $login->fetch();
                $_SESSION['id'] = $info['id'];
                $_SESSION['login'] = true;
                $_SESSION['name'] = $name;
                $_SESSION['password'] = $password;
                $_SESSION['image'] = $info['image'];
                header('Location: '.BASE.'');
            }else{
                echo 'Error';
            }

        }

        public static function getInfosUser(){
            $id_user = $_SESSION['id'];
            $info = \MySql::connect()->prepare("SELECT * FROM `address` WHERE id_user = $id_user");
            $info->execute();
            if(isset($_SESSION['login'])){
                $info = $info->fetch();
                $_SESSION['public_place'] = $info['public_place'];
                $_SESSION['number'] = $info['number'];
                $_SESSION['district'] = $info['district'];
                $_SESSION['id_city'] = $info['id_city'];
            }
        }

    }

?>
