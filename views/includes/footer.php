<footer>
    <div class="wrap w90 center itemsFlex alignCenter">
        <div class="col w50">
            <h4><a>Raissa<span style="color:#3f4579">Dev</span></a></h4>
        </div>
        <div class="col w50 itemsFlex justEnd alignCenter">
            <a href="https://github.com/Raissadev" class="marginRightSmall"><i data-feather="github"></i></a>
            <a href="https://www.linkedin.com/in/raissa-dev-69986a214/" class="marginRightSmall"><i data-feather="linkedin"></i></a>
            <a href="https://www.instagram.com/raissa_dev/"><i data-feather="instagram"></i></a>
        </div>
    </div>
</footer>
<script src="<?php echo BASE; ?>js/scripts.js"></script>
</body>
</html>