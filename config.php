<?php

    session_start();
    date_default_timezone_set('America/Sao_Paulo');

    $autoload = function($class){
        include('classes/'.$class.'.php');
    };

    spl_autoload_register($autoload);

    define('HOST','localhost');
    define('USER','root');
    define('PASSWORD','');
    define('DATABASE','database');
    define('BASE','http://localhost/');
    define('BASE_UPLOADS',__DIR__.'/uploads/');

    if(!isset($_SESSION['login'])){
        $_SESSION['id'] = uniqid();
        $_SESSION['name'] = 'Visitante';
        $_SESSION['password'] = '';
        $_SESSION['image'] = 'avatar.png';
        $_SESSION['public_place'] = null;
        $_SESSION['number'] = null;
        $_SESSION['district'] = null;
        $_SESSION['id_city'] = null;
    }

?>
