<?php
    if(isset($_POST['action'])){
        $api = new \models\ibgeModel;
        $response = $api->getAddress($_POST['uf']);

        $cities = \MySql::connect()->prepare("SELECT * FROM `cities`");
        $cities->execute();

        foreach($response as $key => $value){
            if($cities->rowCount() == 1){
                return;
                echo "<script> alert('Já existe!'); </script>";
            }
            $insertAddress = \MySql::connect()->prepare("INSERT INTO `cities` VALUES (?,?,?)");
            $insertAddress->execute(array($value->id,$value->nome, date('Y-m-d')));
            echo "<script> alert('UF registrado com sucesso!'); </script>";
            header('Location: '.BASE.'register-address-ibge');
            die();
        }
    }
?>

<section class="containerInfo itemsFlex alignCenter justCenter">
    <div class="wrap w90 boxEffect">
        <div class="text w50 marginDownSmall w100Mobile">
            <h2 class="title marginDownSmallIn">Register State In The Database</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam quis dui felis. Integer pulvinar auctor quam. In hac habitasse platea dictumst. Ut id bibendum mauris.</p>
        </div>
        <form method="post">
            <input type="text" name="uf" class="w100 marginDownSmallIn" autocomplete="off" />
            <button type="submit" name="action" class="w20 w50Mobile" >Register State</button>
        </form>
        <div class="marginTopSmall textRight">
            <p>Already own a state? <a href="<?php echo BASE; ?>update-address">Update!</a></p>
        </div>
    </div>
</section>