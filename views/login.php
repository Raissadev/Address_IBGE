<?php
    if(isset($_POST['login'])){
        \models\userModel::login($_POST['name'],$_POST['password']);
    }
    $userController = new controllers\userController();
    $userController::userVerifyer();
?>

<section class="accessContainer itemsFlex alignCenter justCenter">
    <div class="wrap w80 center boxEffect">
        <div class="logo marginDownSmall textCenter">
            <h1 class="title"><a>Raissa<span style="color:#3f4579">Dev</span></a></h1>
        </div>
        <form method="post" class="itemsFlex flexWrap">
            <input type="text" name="name" class="w100 marginDownSmallIn" autocomplete="off" />
            <input type="password" name="password" class="w100 marginDownSmallIn" autocomplete="off" />
            <button name="login" class="w30 center">Logar</button>
        </form>
        <div class="marginTopSmall textRight">
            <p>Não possui uma conta? <a href="<?php echo BASE; ?>register">Clique aqui!</a></p>
        </div>
    </div>
</section>
