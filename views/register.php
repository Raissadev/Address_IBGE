<?php
    if(isset($_POST['register'])){
        \models\userModel::register($_POST['name'],$_POST['password'],$_POST['image']);
    }
    $userController = new controllers\userController();
    $userController::userVerifyer();
?>

<section class="accessContainer itemsFlex alignCenter justCenter">
    <div class="wrap w80 center boxEffect">
        <div class="logo marginDownSmall textCenter">
            <h1 class="title"><a>Raissa<span style="color:#3f4579">Dev</span></a></h1>
        </div>
        <form method="post" enctype="multipart/form-data" class="itemsFlex flexWrap">
            <input type="text" name="name" class="w100 marginDownSmallIn" autocomplete="off" />
            <input type="password" name="password" class="w100 marginDownSmallIn" autocomplete="off" />
            <input type="file" name="image" class="w100 marginDownSmallIn" />
            <button name="register" class="w30 center">Register</button>
        </form>
        <div class="marginTopSmall textRight">
            <p>Já possui uma conta? <a href="<?php echo BASE; ?>login">Clique aqui!</a></p>
        </div>
    </div>
</section>