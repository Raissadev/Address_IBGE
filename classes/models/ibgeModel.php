<?php

    namespace models;

    class ibgeModel{

        private $endpoint;
        private $url;
        
        public function __construct(){
            $this->url = 'https://servicodados.ibge.gov.br/api/v1/localidades';
        }

        public function getAddress($uf){
            $this->endpoint = '/estados/'.$uf.'/municipios';
            $ch = curl_init($this->url . $this->endpoint);

            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt ($ch, CURLOPT_CUSTOMREQUEST, 'GET');

            $response = json_decode(curl_exec($ch));

            curl_close($ch);

            return $response;
        }

        public static function listAddressIBGE($limit){
            $query = '';
            if(isset($_POST['search'])){
                $name = isset($_POST['name']) ? $_POST['name'] : null;
                $identifier = $_POST['identifier'];
                $date = isset($_POST['date']) ? $_POST['date'] : null;
                $query = "WHERE name LIKE '$name%' OR id LIKE '$identifier%'";
            }
            $list = \MySql::connect()->prepare("SELECT * FROM `cities` $query $limit");
            $list->execute();
            $list = $list->fetchAll();
            return $list;
        }

    }

?>